# Generated by Django 3.1.3 on 2020-12-31 21:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0004_comment_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='watchlist',
            name='Product_Name',
        ),
        migrations.AddField(
            model_name='watchlist',
            name='Product_Name',
            field=models.ManyToManyField(related_name='likes', to='auctions.Auction'),
        ),
    ]
