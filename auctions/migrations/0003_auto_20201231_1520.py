# Generated by Django 3.1.3 on 2020-12-31 15:20

import crum
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0002_auto_20201229_1718'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='made_by',
            field=models.ForeignKey(blank=True, default=crum.get_current_user, on_delete=django.db.models.deletion.CASCADE, related_name='comments', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='auction',
            name='author',
            field=models.ForeignKey(blank=True, default=crum.get_current_user, on_delete=django.db.models.deletion.CASCADE, related_name='listings', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='bid',
            name='BidAuthor',
            field=models.ForeignKey(blank=True, default=crum.get_current_user, on_delete=django.db.models.deletion.CASCADE, related_name='bids', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='comment',
            name='Content',
            field=models.TextField(blank=True),
        ),
        migrations.CreateModel(
            name='WatchList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Product_Name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='likes', to='auctions.auction')),
                ('author', models.ForeignKey(blank=True, default=crum.get_current_user, on_delete=django.db.models.deletion.CASCADE, related_name='followed_listings', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
